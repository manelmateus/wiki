# Realização de UC5 Especificar Área Geográfica

## Racional

| Fluxo Principal                                                                                        | Questão: Que Classe...                                      | Resposta                                       | Justificação                                                                                                         |
|:-------------------------------------------------------------------------------------------------------|:------------------------------------------------------------|:-----------------------------------------------|:---------------------------------------------------------------------------------------------------------------------|
| 1. O administrativo inicia a especificação de uma nova área geográfica. | ... interage com o utilizador? | EspecificarAreaGeograficaUI.                          | Pure Fabrication, pois não se justifica atribuir esta responsabilidade a nenhuma classe existe no Modelo de Domínio. |
|| ... coordena o UC?                                                                              | EspecificarAreaGeograficaController                                | Controller.                                    |                                                                                                                      |
|| ... cria/instancia área geográfica?          | Empresa                                               | Creator (Regra 1)                              |                                                                                                                      |
| 2.	O sistema solicita os dados necessários (i.e. designação e custo de deslocação). |                  |                                                |                                                                                                                      |
| 3.	O administrativo introduz os dados solicitados.   | ... guarda os dados introduzidos?                    |Área Geográfica                                    | Information Expert (IE) - instância criada no passo 1                                                                                              |
| 4.	 O sistema valida e apresenta os dados ao administrativo, pedindo que os confirme.                                                             | ... valida os dados da área geográfica (validação local)? | Área Geográfica                                     | IE: Área Geográfica possui os seus próprios dados                                                                                                                   |
|| ... valida os dados da Área Geográfica (validação global)?                                           | Empresa                                               | IE: A Empresa contém/agrega AreasGeograficas |                                                                                                                      |
| 5. O administrativo confirma.                                                                     |                                                             |                                                |                                                                                                                      |
| 6.	O sistema regista os dados e informa o administrativo do sucesso da operação.                           | ... guarda a área geográfica  especificada/criada?                            | Empresa                                 | IE. No MD a Empresa atua em várias áreas geográficas                                                                |
|| ... notifica o utilizador?                                                                                   | EspecificarCategoriaUI                                        |                                                |                                                                                                                      |

## Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Empresa
 * Área Geográfica

Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * EspecificarAreaGeograficaUI  
 * EspecificarAreaGeograficaController


##	Diagrama de Sequência
![SD_UC5_AreaGEografica.jpg](SD_UC5_AreaGEografica.jpg)



##	Diagrama de Classes
![DC_UC5_AreaGeografica.jpg](DC_UC5_AreaGeografica.jpg)