#Glossário

| **_Termo_**                   	| **_Descrição_**                                           |                                       
|:------------------------|:----------------------------------------------------------------|
| **Administrativo** | Pessoa responsável pela realização de várias e diferentes atividades de suporte ao funcionamento da empresa.|
| **Área Geográfica** | Corresponde a uma determinada região da superfície terrestre na qual a empresa atua ou pode atuar (i.e. prestar serviços). A prestação de serviços numa dada área geográfica implica um custo de deslocação.|
| **Candidatura a Prestador de Serviço** | Corresponde a uma conjunto de informação disponibilizada por uma pessoa à empresa com o objetivo de futuramente ser poder tornar um prestador de serviços da empresa.|
| **Categoria de Serviço** | Corresponde a uma descrição usada para catalogar um ou mais serviços (semelhantes) prestados pela empresa.|
| **Cliente** | Pessoa que interage com a aplicação com o intuito principal de efetuar pedidos de prestação de serviço(s) e consequentemente, usufruir dos beneficios desses mesmos serviços.|
| **Empresa** | Organização que se dedica à prestação de serviços ao domicilio variados e para a qual a aplicação informática está a ser desenvolvida. |
| **FRH** | Acrónimo para Funcionário de Recursos Humanos.|
| **Funcionário de Recursos Humanos** | Pessoa responsável pela realização de várias e diferentes atividades de suporte ao funcionamento da empresa.|
| **Pedido de Prestação de Serviço** | Corresponde a uma solicitação realizada pelo cliente à empresa com o intuito de esta lhe prestar os serviços pretendidos dentro de um ou mais período de tempo a troco de um determinado custo (estimado).|
| **Prestador de Serviços** | Pessoa responsável por executar os serviços solicitados pelos clientes à empresa. |
| **Processo de Autenticação** | Meio através do qual se procede à verificação da identidade da pessoa que pretende/está a utilizar a aplicação informática.|
| **Serviço** | Corresponde à descrição de uma atividade/tarefa que a empresa se propõe prestar aos seus clientes recorrendo a uma ou mais pessoas (i.e. Prestador de Serviços).|
| **Utilizador** | Pessoa que interage com a aplicação informática|
| **Utilizador Não Registado** | Utilizador que interage com a aplicação informática de forma anónima, i.e. sem ter realizado previamente o processo de autenticação previsto.|
| **Utilizador Registado** | Utilizador que interage com a aplicação informática após ter realizado o processo de autenticação previsto e, portanto, a aplicação conhece a sua identidade. Tipicamente, este assume o papel/função de Cliente ou Administrativo ou de FRH ou Prestador de Serviços.|








