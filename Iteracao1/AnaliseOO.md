# Análise OO #
O processo de construção do modelo de domínio é baseado nos casos de uso, em especial os substantivos utilizados, e na descrição do enunciado.
## Racional para identificação de classes de domínio ##
Para a identificação de classes de domínio usa-se a lista de categorias das aulas TP (sugeridas no livro). Como resultado temos a seguinte tabela de conceitos (ou classes, mas não de software) por categoria.

### _Lista de Categorias_ ###

**Transações (do negócio)**

* Pedido Prestação Serviço;
* Candidatura


---

**Linhas de transações**

* Preferência Horário
* Descrição Serviço Pedido 
* Outro Custo Adicional
---

**Produtos ou serviços relacionados com transações**

*  Serviço

---


**Registos (de transações)**

*  

---  


**Papéis das pessoas**

* Administrativo
* Funcionário Recursos Humanos (FRH)
* Cliente
* Prestador de Serviços
* Utilizador
* Utilizador Não Registado

---


**Lugares**

*  Área Geográfica
*  Endereço Postal

---

**Eventos**

* 

---


**Objectos físicos**

*

---


**Especificações e descrições**

*  (Especificar) Categoria (de Serviço)
*  (Especificar) Serviço
*  Habilitação Académica
*  Habilitação Profissional

---


**Catálogos**

*  

---


**Conjuntos**

*  

---


**Elementos de Conjuntos**

*  

---


**Organizações**

*  Empresa

---

**Outros sistemas (externos)**

*  (Componente Gestão Utilizadores)

---


**Registos (financeiros), de trabalho, contractos, documentos legais**

* 

---


**Instrumentos financeiros**

*  

---


**Documentos referidos/para executar as tarefas/**

* Documento Comprovativo   

---



###**Racional sobre identificação de associações entre classes**###

Uma associação é uma relação entre instâncias de objectos que indica uma conexão relevante e que vale a pena recordar, ou é derivável da Lista de Associações Comuns:

+ A é fisicamente (ou logicamente) parte de B
+ A está fisicamente (ou logicamente) contido em B
+ A é uma descrição de B
+ A é conhecido/capturado/registado por B
+ A usa ou gere B
+ A está relacionado com uma transacção de B + etc.



| Conceito (A) 		|  Associação   		|  Concept (B) |
|----------	   		|:-------------:		|------:       |
| Administrativo  	| especifica     	| Categoria  |
|   					| especifica     	| Serviço  |
|   					| especifica     	| Área Geográfica  |
|   					| trabalha para     | Empresa  |
|						| atua como			| Utilizador |
| Empresa				| presta     			| Serviço  |
|						| tem     			| Categoria  |
|						| atua em    			| Área Geográfica  |
| 						| possui     			| Cliente  |
| 						| possui     			| Administrativo  |
| 						| possui     			| FRH  |
| 						| possui     			| Prestador de Serviços  |
| 						| recebe     			| Pedido de Prestação de Serviços  |
| 						| recebe     			| Candidatura a Prestador de Serviços   |
| Serviço				| catalogado em     | Categoria  |
|						| é solicitado em	| Pedido de Prestação de Serviços |
|						| referido em			| Descrição Serviço Pedido |
| Cliente				| possui     			| Endereço Postal  |
|						| atua como			| Utilizador |
|						| realiza				| Pedido de Prestação de Serviços |
| Categoria			| cataloga    		| Serviço  |
|						| mencionada em     | Candidatura a Prestador de Serviços  |
| Prestador de Serviços| atua como		| Utilizador |
| FRH					| atua como		| Utilizador |
| Candidatura a Prestador de Serviços |  menciona | Endereço Postal
| 						|  menciona 		| Habilitação Académica
| 						|  menciona 		| Habilitação Profissional
| 						|  menciona 		| Categoria de Serviço
| 						|  tem anexado 	| Documento Comprovativo
| Pedido de Prestação de Serviços 	| é feito por | Cliente |
| 					   | inclui 			| Outro Custo Adicional |
| 					   | indica 			| Preferência Horário |
| 					   | possui 			| Descrição Serviço Pedido |
|						| para realizar-se em|| Endereço Postal |
| Descrição Serviço Pedido	 | consta	| Pedido de Prestação de Serviços |
| 								 | referente |  Serviço |



## Modelo de Domínio
### Alternativa A
![MD_IT1.png](MD_IT1_vA.png)

### Alternativa B
![MD_IT1.png](MD_IT1_vB.png)

**Nota 1:** As alternativas A e B diferem apenas na forma de representação da associação entre "Pedido de Prestação de Serviços", "Descrição Serviço Pedido" e "Serviço". As duas formas usadas são semânticamente idênticas. No contexto da alternativa B, designa-se a classe "Descrição Serviço Pedido" como sendo uma **classe associativa**. Tipicamente, as classes associativas surgem quando é necessário detalhar (i.e. recolher mais informação) associações de muitos para muitos (\*:*) entre duas classes.

**Nota 2:** Os atributos de "Habilitação Académica" e de "Habilitação Profissional" não foram indicados nem explicitamente nem implicitamente pelo cliente. Como tal, deve-se questionar o cliente sobre isto de modo a esclarecer as suas reais necessidades. Enquanto isso não acontece, e recorrendo ao conhecimento empírico, adotou-se de forma muito minimalista alguns atributos para efeitos de demonstração.  