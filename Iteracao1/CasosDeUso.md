# Diagrama de Casos de Uso

![Diagrama de Casos de Uso](CasosUso/DUC_IT1.png)

# Casos de Uso
| UC  | Descrição                                                               |                   
|:----|:------------------------------------------------------------------------|
| UC1 | [Efetuar Registo como Cliente](CasosUso/UC1_EfetuarRegistoCliente.md)   |
| UC2 | [Submeter Candidatura a Prestador Serviço](CasosUso/UC2_SubmeterCandidaturaPrestadorServico.md)  |
| UC3 | [Especificar Categoria (de Serviço)](CasosUso/UC3_EspecificarCategoria.md)|
| UC4 | [Especificar Serviço](CasosUso/UC4_EspecificarServico.md)|
| UC5 | [Especificar Área Geográfica](CasosUso/UC5_EspecificarAreaGeografica.md) |
| UC6 | [Efetuar Pedido Prestação de Serviços](CasosUso/UC6_EfetuarPedidoPrestacaoServicos.md)|