# Projeto de ESOFT 2018-2019 - Proposta de Resolução

* [Iteração 1 / Iteration 1](Iteracao1/Home)
* [Iteração 2 / Iteration 2](Iteracao2/Home)
* [Iteração 3 / Iteration 3](Iteracao3/Home)

**Aviso:** a resolução disponibilizada pode conter (algumas) gralhas. Na eventualidade de (considerar que) encontrar alguma gralha, debata a mesma com o docente das aulas práticas, teórico-práticas ou teóricas.